[![pipeline status](https://gitlab.com/test-automation-university1/api-python-path/introduction-to-pytest/tau-intro-to-pytest/badges/master/pipeline.svg)](https://gitlab.com/test-automation-university1/api-python-path/introduction-to-pytest/tau-intro-to-pytest/-/commits/master)
[![coverage report](https://gitlab.com/test-automation-university1/api-python-path/introduction-to-pytest/tau-intro-to-pytest/badges/master/coverage.svg)](https://gitlab.com/test-automation-university1/api-python-path/introduction-to-pytest/tau-intro-to-pytest/-/commits/master)

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=tau-intro-to-pytest&metric=alert_status)](https://sonarcloud.io/dashboard?id=tau-intro-to-pytest)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=tau-intro-to-pytest&metric=coverage)](https://sonarcloud.io/dashboard?id=tau-intro-to-pytest)

# tau-intro-to-pytest

Sections of the course:

## Chapter 1: The First Test Case

## Chapter 2: A Failing test case

## Chapter 3: A Test Case with an Exception

## Chapter 4: A Parametrized Test Case

Example:

````cython
import pytest

products = [
    (2, 3, 6),
    (1, 99, 99),
    (0, 45, 0),
    (3, -4, -12),
    (3, -4, -12),
    (-4, -4, 16),
    (2.5, 6.7, 16.75),
]

@pytest.mark.parametrize("a, b, product", products)
def test_multiplication(a, b, product):
    assert a * b == product
````

## Chapter 5: Testing Classes

## Chapter 6: Fixtures

Scope of the fixture. If the scope is set to `session`, the fixture is executed only once for the entire test suite.

It is interesting for a fixture function that reads data from an external file.

## Chapter 7: Commands anf Configs

## Chapter 8: Filtering Tests

## Chapter 9: Feature Tests

[DuckDuckGo][1] API.

## Chapter 10: Extending pytest with plugins

* pytest-cov: coverage reports
* pytest-html: html reports
* pytest-xdist: Tests in parallel
* pytest-bdd: BDD testing


[1]: https://duckduckgo.com/api

