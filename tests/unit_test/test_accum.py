import pytest


@pytest.mark.accumulator
def test_accumulator_init(accum):
    assert accum.count == 0


@pytest.mark.accumulator
def test_accumulator_add_one(accum):
    accum.add()

    assert accum.count == 1


@pytest.mark.accumulator
def test_accumulator_add_three(accum):
    accum.add(3)

    assert accum.count == 3


@pytest.mark.accumulator
def test_accumulator_add_twice(accum):
    accum.add(3)
    accum.add(3)

    assert accum.count == 6


@pytest.mark.accumulator
def test_accumulator_cannot_set_account_directly(accum):
    with (pytest.raises(AttributeError, match="can't set attribute")):
        accum.count = 10
