import pytest


@pytest.mark.math
def test_one_plus_one():
    assert 1 + 1 == 2


products = [
    (2, 3, 6),
    (1, 99, 99),
    (0, 45, 0),
    (3, -4, -12),
    (3, -4, -12),
    (-4, -4, 16),
    (2.5, 6.7, 16.75),
]


@pytest.mark.math
@pytest.mark.parametrize("a, b, product", products)
def test_multiplication(a, b, product):
    assert a * b == product
