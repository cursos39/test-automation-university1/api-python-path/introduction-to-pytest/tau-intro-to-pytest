#!/bin/sh

python -m coverage run --source . -m pytest \
  && python -m coverage report --omit=tests/*.py,setup.py \
  && python -m coverage xml -o coverage-reports/coverage.xml --omit=tests/*.py,setup.py \
  && python -m coverage html -d coverage-reports/html --omit=tests/*.py,setup.py